import pygame as pg
import sys

tama = 500,500
ventana = pg.display.set_mode(tama) #Surface
pg.display.set_caption("Mi ventana")

obj1 = pg.image.load("imagenes/nave.png") #Surface
obj1 = pg.transform.scale(obj1, (100,100))
rectObj1 = obj1.get_rect()

obj2 = pg.image.load("imagenes/nave.png")
obj2 = pg.transform.scale(obj2, (100,100))
rectObj2 = obj2.get_rect()
rectObj2.left = 100
rectObj2.top = 100

desplazamiento = 5
sentidoNave = "arr"

# ventana.blit(obj1, (0,0))

while True:
    ventana.fill((0,0,0), rectObj1)
    for evento in pg.event.get():
        if evento.type == pg.QUIT or (evento.type == pg.KEYDOWN and evento.key == pg.K_ESCAPE):
            sys.exit()
            pg.quit()
        if evento.type == pg.KEYDOWN:
            if evento.key == pg.K_UP:
                rectObj1.top -= desplazamiento
                if sentidoNave == "izq":
                    obj1 = pg.transform.rotate(obj1, -90)
                if sentidoNave == "der":
                    obj1 = pg.transform.rotate(obj1, 90)
                if sentidoNave == "aba":
                    obj1 = pg.transform.rotate(obj1, 180)
                sentidoNave = "arr"
            if evento.key == pg.K_DOWN:
                rectObj1.top += desplazamiento
                if sentidoNave == "izq":
                    obj1 = pg.transform.rotate(obj1, 90)
                if sentidoNave == "der":
                    obj1 = pg.transform.rotate(obj1, -90)
                if sentidoNave == "arr":
                    obj1 = pg.transform.rotate(obj1, 180)
                sentidoNave = "aba"
            if evento.key == pg.K_LEFT:
                rectObj1.left -= desplazamiento
                if sentidoNave == "aba":
                    obj1 = pg.transform.rotate(obj1, -90)
                if sentidoNave == "der":
                    obj1 = pg.transform.rotate(obj1, 180)
                if sentidoNave == "arr":
                    obj1 = pg.transform.rotate(obj1, 90)
                sentidoNave = "izq"            	

            if evento.key == pg.K_RIGHT:
                rectObj1.left += desplazamiento
                if sentidoNave == "izq":
                    obj1 = pg.transform.rotate(obj1, 180)
                if sentidoNave == "aba":
                    obj1 = pg.transform.rotate(obj1, 90)
                if sentidoNave == "arr":
                    obj1 = pg.transform.rotate(obj1, -90)
                sentidoNave = "der"            	

    # if rectObj1.left <= 0 or rectObj1.right >= tama[0]:
    #     desplazamiento *= -1

    # rectObj1 = rectObj1.move((desplazamiento, 0))
    ventana.blit(obj1, rectObj1)
    ventana.blit(obj2, rectObj2)
    if rectObj1.colliderect(rectObj2):
        print("choque")
    
    pg.time.delay(50)
    pg.display.flip()