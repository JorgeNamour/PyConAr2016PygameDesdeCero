[PyConAr 2016] Bahía Blanca - Buenos Aires - Argentina

Taller: "Pygame desde CERO"

Descripción: 
"Pygame desde CERO" es un taller destinado al público en general. Daremos un recorrido desde la historia de Python, SDL y Pygame, pasando por el uso e implementación de varias funcionalidades de Pygame (crear ventana, insertar, mover, girar y colisionar objetos), además, desarrollaremos entre todos los presentes el famoso juego “Snake” utilizando una plantilla para manejo de múltiples pantallas.
